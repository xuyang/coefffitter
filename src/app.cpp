#include "app.hpp"
#include "RooDataHist.h"
#include "RooGlobalFunc.h"
#include "RooPlot.h"
#include "RooRealVar.h"
#include "RtypesCore.h"
#include "TCanvas.h"
#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <vector>
#include "RooFitResult.h"
#include "RooMinuit.h"
#include "models.hpp"

Application::Application(TFile* infile) : rfile(infile)
{
  vec_model.clear();
}

int Application::prepareModel(std::string m_name ,std::string model_name)
{
  if (model_name == "Func_Ctheta") {
    vec_model[m_name] = new Func_Ctheta();
  } else if (model_name == "Func_Phi") {
    vec_model[m_name] = new Func_Phi();
  }
  else {
    std::cout << "cannot find the model " << model_name << std::endl;
    return 0;
  }
  vec_name.push_back(m_name);
  current_name = m_name;
  return 1;
}

int Application::prepareModel(std::string m_name, FitConfig* config)
{
  auto type = config->type;
  std::string model_name;

  switch (type) {
  case FUNCTION_COS_THETA:
      model_name = "Func_Ctheta";
      break;
  case FUNCTION_PHI:
      model_name = "Func_Phi";
      break;
  case HIST_TEMPLATE_COS_THETA:
      model_name = "Hist_Ctheta";
      break;
  case HIST_TEMPLATE_PHI:
      model_name = "Hist_Phi";
      break;
  default:
      std::cout << "given pattern is not found" << std::endl;
      return -1;
  }
  std::cout<<model_name<<std::endl;
  if (model_name == "Func_Ctheta") {
    vec_model[m_name] = new Func_Ctheta();
  } else if (model_name == "Func_Phi") {
    vec_model[m_name] = new Func_Phi();
  }
  else if (model_name == "Hist_Ctheta") {
    auto template_file = config->template_file;
    auto template_list = config->template_list;
    auto template_name = template_list[m_name];
    vec_model[m_name] = new Hist_Ctheta(template_file, template_name);
  }
  else if (model_name == "Hist_Phi") {
  auto template_file = config->template_file;
  auto template_list = config->template_list;
  auto template_name = template_list[m_name];
  vec_model[m_name] = new Hist_Phi(template_file, template_name);
  }
  else {
    std::cout << "cannot find the model " << model_name << std::endl;
    return 0;
  }
  vec_name.push_back(m_name);
  current_name = m_name;
  return 1;
}

int Application::runFit(const char* hist_name) {

  std::cout << "run fit for " << current_name << std::endl;

  auto hist = rfile->Get<TH1D>(hist_name);

  auto x = vec_model[current_name]->GetVar();
  x->Print();
  auto fitModel = vec_model[current_name]->GetModel();
  // auto res = vec_model[current_name]->GetResultPrt();

  auto dh = new RooDataHist("data", "data", *x, RooFit::Import(*hist));
  dh->Print();

  /*auto nll = fitModel->createNLL(*dh);
  RooMinuit m(*nll);
  m.setVerbose(kFALSE);
  m.minos();
  fitModel->getParameters(*x)->Print("s");*/

  //vec_result[current_name] = fitModel->fitTo(*dh, RooFit::Save(kTRUE), RooFit::SumW2Error(true));
  vec_result[current_name] = fitModel->chi2FitTo(*dh, RooFit::Range("fitRange"), RooFit::SumW2Error(false), RooFit::Save(kTRUE));
  // auto res = fitModel->fitTo(
  //    *dh, RooFit::Extended(1), RooFit::Range("fitRange"),
  //    RooFit::SumCoefRange("fitRange"), RooFit::NumCPU(4), RooFit::Verbose(1),
  //    RooFit::PrintLevel(1), RooFit::Save(kTRUE));

  auto compList = vec_model[current_name]->getCompList();
  auto frame = x->frame(RooFit::Title(current_name.c_str()));
  TCanvas *c1 = new TCanvas("c1", "", 800, 600);
  float max = hist->GetMaximum();
  frame->SetMinimum(0);
  frame->SetMaximum(1.5*max);
  c1->Draw();
  dh->plotOn(frame);
  fitModel->plotOn(frame);

  /*int iColor = 3;
  for (auto comp : compList)
  {
    fitModel->plotOn(frame, RooFit::Components(comp.c_str()), RooFit::LineStyle(kDashed), RooFit::LineColor(iColor++));
  }*/
  frame->Draw();

  // c1->Print("test.png");
  c1->Print((current_name+".png").c_str());

  vec_result[current_name]->Print();

  return 1;
}

// only call it when finished everything for one fit
/* void Application::reset()
{
  delete model;
} */

int Application::getResult(TFile* outfile)
{

  int nBins = vec_name.size();
  std::vector<std::string> parList = vec_model[current_name]->getParList();
  int nHists = parList.size();
  outfile->cd();
  std::map<std::string, TH1F*> mResultHists;

  for (auto par : parList)
  {
    mResultHists[par] = new TH1F(par.c_str(), "", nBins, 0, nBins);
  }
  int i_bin = 1;
  for (auto job : vec_name)
  {
    // auto _model = vec_model.at(job);
    auto mRes = vec_result.at(job);
     // first get norm parameter, it is required and name should be "Norm"
    mRes->Print();
    // auto Norm = (RooRealVar*)mRes->floatParsFinal().find("Norm");
    // float Norm_val = Norm->getValV();
    // float Norm_err = Norm->getError();

    std::map<std::string, float> bareVal;
    std::map<std::string, float> bareErr;

    float Norm_val = 1.0;
    float Norm_err = 0.0;
    //auto norm_par = (RooRealVar*)mRes->floatParsFinal().find("Norm");
    //auto Norm_val = norm_par->getValV();
    //auto Norm_err = norm_par->getError();
    for (auto par_name : parList)
    {
      auto par = (RooRealVar*)mRes->floatParsFinal().find(par_name.c_str());
      bareVal[par_name] = par->getValV();
      bareErr[par_name] = par->getError();
      Norm_val -= par->getValV();
      Norm_err += pow(par->getError(), 2);
    }
    Norm_err = std::sqrt(Norm_err);
    // print some basic info
    std::cout << job << ": " << std::endl;
    std::cout<<"Norm: " << Norm_val << "+/-" << Norm_err << std::endl;

    for (auto par_name : parList)
    {
      auto rval = calculateRealValue(bareVal[par_name], bareErr[par_name], Norm_val, Norm_err);
      mResultHists[par_name]->SetBinContent(i_bin, rval[0]);
      mResultHists[par_name]->SetBinError(i_bin, rval[1]);
      std::cout << "\t" << par_name << ": " << rval[0] << " +/- " << rval[1] << std::endl;
    }


  /*  for (auto par_name : parList)
    {
      if (par_name == "Norm") continue;
      auto par = (RooRealVar*)mRes->floatParsFinal().find(par_name.c_str());
      float par_val = par->getValV();
      float par_err = par->getError();

      auto rval = calculateRealValue(par_val, par_err, Norm_val, Norm_err);
      mResultHists[par_name]->SetBinContent(i_bin, rval[0]);
      mResultHists[par_name]->SetBinError(i_bin, rval[1]);

      std::cout << "\t" << par_name << ": " << rval[0] << " +/- " << rval[1] << std::endl;
    } */
    i_bin++;
  }
  for (auto par_name : parList)
  {
    mResultHists[par_name]->Write("", TObject::kOverwrite);
  }
  // outfile->Write();
  return 1;
}

std::map<std::string, FitConfig*> getConfigList(std::string fpath)
{
  std::map<std::string, FitConfig*> res;
  // check if a customized config file is given
  if (fpath == "")
  {
    std::cout << "you must give a path to the config file" << std::endl;
    return res;
  }

  std::ifstream in(fpath.c_str());
  std::string line;
  std::string current_config;

  while (getline(in, line))
  {
    if (line == "") continue;
    std::stringstream ss(line);
    int nspace = std::count(line.begin(), line.end(), ' ');
    std::vector<std::string> tmpS;
    tmpS.resize(nspace+1);
    for (int i = 0; i < nspace+1; ++i)
    {
      ss >> tmpS[i];
      std::cout<<tmpS[i]<<std::endl;
    }

    // check if a new config start
    if (tmpS[0] == "CONFIG" && tmpS[1] != "")
    {
      res[tmpS[1]] = new FitConfig;
      current_config = tmpS[1];
      continue;
    }

    if (tmpS[0] == "TYPE" && tmpS[1] != "")
    {
      if (tmpS[1] == "Func_Ctheta")
      {
        res[current_config]->type = FUNCTION_COS_THETA;
        continue;
      }
      else if (tmpS[1] == "Func_Phi") 
      {
        res[current_config]->type = FUNCTION_PHI;
        continue;
      }
      else if (tmpS[1] == "Hist_Ctheta") 
      {
        res[current_config]->type = HIST_TEMPLATE_COS_THETA;
        continue;
      }
      else if (tmpS[1] == "Hist_Phi")
      {
        res[current_config]->type = HIST_TEMPLATE_PHI;
        continue;
      }
      else
      {
        {
          std::cout << "wrong type name" << std::endl;
        }
      }
    }

    if (tmpS[0] == "HIST" && tmpS[1] != "" && tmpS[2] != "")
    {
      res[current_config]->hist_list[tmpS[1]] = tmpS[2];
      continue;
    }
    if (tmpS[0] == "TEMPLATE_FILE" && tmpS[1] != "")
    {
      res[current_config]->template_file = tmpS[1];
      continue;
    }
    if (tmpS[0] == "TEMPLATE" && tmpS[1] != "" && tmpS[2] != "")
    {
      res[current_config]->template_list[tmpS[1]] = tmpS[2];
    }
  }

  return res;
}
