#include "models.hpp"
#include "RooAbsReal.h"
#include "RooArgList.h"
#include "RooDataHist.h"
#include "RooFitResult.h"
#include "RooFormulaVar.h"
#include "RooRealSumPdf.h"
#include "RooRealVar.h"
#include "TF1.h"
#include "TH1.h"
#include "TCanvas.h"
#include "RooTFnBinding.h"
#include <cmath>
#include <iostream>
#include <map>
#include <vector>
#include "RooRealSumFunc.h"
#include "TMath.h"
#include "TMatrixFfwd.h"
#include "TFile.h"
#include "RooHistFunc.h"

TString Welcome()
{
    return TString("Hello World!");
}

RooRealVar* Model_Base::GetVar()
{
    return x;
}

/*RooRealSumFunc* Model_Base::GetModel()
{
    return model;
}*/

RooRealSumPdf* Model_Base::GetModel()
{
    return model;
}

std::vector<std::string> Model_Base::getParList()
{
    return mParList;
}

std::vector<std::string> Model_Base::getCompList()
{
    return mCompList;
}
/* RooFitResult* Model_Base::GetResultPrt()
{
    return mRes;
} */


Func_Ctheta::Func_Ctheta()
{
    f_P0 = new TF1("f_P0", "0.5*(1-3*x*x)", -1, 1);
    f_P4 = new TF1("f_P4", "x", -1, 1);
    f_P8 = new TF1("f_P8", "1+x*x", -1, 1);

    x = new RooRealVar("ctheta", "cos(#theta)", -1, 1);

    RooAbsReal *P0 = RooFit::bindFunction(f_P0, *x);
    RooAbsReal *P4 = RooFit::bindFunction(f_P4, *x);
    RooAbsReal *P8 = RooFit::bindFunction(f_P8, *x);

    P0->Print();
    P4->Print();
    P8->Print();

    func_list = new RooArgList();
    func_list->add(*P0);
    func_list->add(*P4);
    func_list->add(*P8);

    A0 = new RooRealVar("A0", "A_0", 0.01, -10.0, 10.0);
    A4 = new RooRealVar("A4", "A_4", 0.01, -10.0, 10.0);
    //A8 = new RooRealVar("Norm", "A_8", 0.01, -10000.0, 100000.0);
    A8 = new RooFormulaVar("Norm", "1.0 -@0 -@1", RooArgList(*A0, *A4));

    coeff_list = new RooArgList();
    coeff_list->add(*A0);
    coeff_list->add(*A4);
    coeff_list->add(*A8);

    mParList.push_back("A0");
    mParList.push_back("A4");
    // mParList.push_back("Norm");

    mCompList.push_back("f_P0");
    mCompList.push_back("f_P4");
    mCompList.push_back("f_P8");


    model = new RooRealSumPdf("ctheta_pdf", "ctheta_pdf", *func_list, *coeff_list);
    model->printCompactTree();

    std::cout << "Initialisation of cos theta pdf done..." << std::endl;
}

/*std::vector<std::string> Func_Ctheta::getParList()
{
    return mParList;
}*/

Func_Phi::Func_Phi()
{
    float pi = TMath::Pi();
    f_P2 = new TF1("f_P2", "0.25*cos(2*x)", 0, 2*pi);
    f_P3 = new TF1("f_P3", "(3.0*TMath::Pi()/16.0)*cos(x)", 0, 2*pi);
    f_P9 = new TF1("f_P9", "1", 0, 2*pi);

    x = new RooRealVar("phi", "#phi", 0, 2*pi);

    RooAbsReal* P2 = RooFit::bindFunction(f_P2, *x);
    RooAbsReal* P3 = RooFit::bindFunction(f_P3, *x);
    RooAbsReal* P9 = RooFit::bindFunction(f_P9, *x);

    func_list = new RooArgList();
    func_list->add(*P2);
    func_list->add(*P3);
    func_list->add(*P9);

    A2 = new RooRealVar("A2", "A_2", 0.01, -10.0, 10.0);
    A3 = new RooRealVar("A3", "A_3", 0.01, -10.0, 10.0);
    //A9 = new RooRealVar("Norm", "A_9", 0.01, -100000.0, 100000.0);
    A9 = new RooFormulaVar("Norm", "1.0 -@0 -@1", RooArgList(*A2, *A3));

    coeff_list = new RooArgList();
    coeff_list->add(*A2);
    coeff_list->add(*A3);
    coeff_list->add(*A9);

    mParList.push_back("A2");
    mParList.push_back("A3");

    mCompList.push_back("f_P2");
    mCompList.push_back("f_P3");
    mCompList.push_back("f_P9");

    model = new RooRealSumPdf("phi_pdf", "phi_pdf", *func_list, *coeff_list);
    std::cout << "Initialisation of phi pdf done..." << std::endl;
}

/*std::vector<std::string> Func_Phi::getParList()
{
    return mParList;
}*/

/* int Func_Ctheta::getPar(std::map<std::string, float> &mPar, std::map<std::string, float> &mErr)
{
    if (mRes == nullptr)
    {
        std::cout << "fit has not been done! " << std::endl;
        return 0;
    }
    // first get norm parameter, it is required and name should be "Norm"
    auto Norm = (RooRealVar*)mRes->floatParsFinal().find("Norm");
    float Norm_val = Norm->getVal();
    float Norm_err = Norm->getError();

    for (auto var_name : mParList)
    {
        if (var_name == "Norm") continue;
        auto var = (RooRealVar*)mRes->floatParsFinal().find(var_name.c_str());
        float var_val = var->getVal();
        float var_err = var->getError();

        auto rval = calculateRealValue(var_val, var_err, Norm_val, Norm_err);
        mPar[var_name] = rval[0];
        mErr[var_name] = rval[1];

        std::cout << var_name << ": " << rval[0] << " +/- " << rval[1] << std::endl;
    }
    return 1;
} */

Hist_Ctheta::Hist_Ctheta(std::string template_path, std::string template_name)
{
    x = new RooRealVar("ctheta", "cos(#theta)", -1, 1);
    TFile *template_file = TFile::Open(template_path.c_str(), "READONLY");

    hist_P0 = template_file->Get<TH1D>((template_name+"_P0").c_str());
    hist_P4 = template_file->Get<TH1D>((template_name+"_P4").c_str());
    hist_P8 = template_file->Get<TH1D>((template_name+"_P8").c_str());

    auto dh_P0 = new RooDataHist("dh_P0", "dh_P0", *x, RooFit::Import(*hist_P0));
    auto dh_P4 = new RooDataHist("dh_P4", "dh_P4", *x, RooFit::Import(*hist_P4));
    auto dh_P8 = new RooDataHist("dh_P8", "dh_P8", *x, RooFit::Import(*hist_P8));

    auto template_P0 = new RooHistFunc("template_P0", "template_P0", *x, *dh_P0);
    auto template_P4 = new RooHistFunc("template_P4", "template_P4", *x, *dh_P4);
    auto template_P8 = new RooHistFunc("template_P8", "template_P8", *x, *dh_P8);

    template_list = new RooArgList();
    template_list->add(*template_P0);
    template_list->add(*template_P4);
    template_list->add(*template_P8);

    A0 = new RooRealVar("A0", "A_0", 0.01, -10.0, 10.0);
    A4 = new RooRealVar("A4", "A_4", 0.01, -10.0, 10.0);
    //A8 = new RooRealVar("Norm", "A_8", 0.01, 0.0, 100000.0);
    A8 = new RooFormulaVar("Norm", "1.0 -@0 -@1", RooArgList(*A0, *A4));

    coeff_list = new RooArgList();
    coeff_list->add(*A0);
    coeff_list->add(*A4);
    coeff_list->add(*A8);

    mParList.push_back("A0");
    mParList.push_back("A4");

    mCompList.push_back("template_P0");
    mCompList.push_back("template_P4");
    mCompList.push_back("template_P8");

    model = new RooRealSumPdf("template_ctheta_pdf", "template_ctheta_pdf", *template_list, *coeff_list);

}

/*std::vector<std::string> Hist_Ctheta::getParList()
{
    return mParList;
}*/


Hist_Phi::Hist_Phi(std::string template_path, std::string template_name)
{
    float pi = TMath::Pi();
    x = new RooRealVar("phi", "#phi", 0, 2*pi);
    x->setBins(8);
    TFile *template_file = TFile::Open(template_path.c_str(), "READONLY");

    hist_P2 = template_file->Get<TH1D>((template_name+"_P2").c_str());
    hist_P3 = template_file->Get<TH1D>((template_name+"_P3").c_str());
    hist_P9 = template_file->Get<TH1D>((template_name+"_P9").c_str());
    //hist_P5 = template_file->Get<TH1D>((template_name+"_P5").c_str());
    //hist_P7 = template_file->Get<TH1D>((template_name+"_P7").c_str());

    auto dh_P2 = new RooDataHist("dh_P2", "dh_P2", *x, RooFit::Import(*hist_P2));
    auto dh_P3 = new RooDataHist("dh_P3", "dh_P3", *x, RooFit::Import(*hist_P3));
    auto dh_P9 = new RooDataHist("dh_P9", "dh_P9", *x, RooFit::Import(*hist_P9));
    //auto dh_P5 = new RooDataHist("dh_P5", "dh_P5", *x, RooFit::Import(*hist_P5));
    //auto dh_P7 = new RooDataHist("dh_P7", "dh_P7", *x, RooFit::Import(*hist_P7));

    auto template_P2 = new RooHistFunc("template_P2", "template_P2", *x, *dh_P2);
    auto template_P3 = new RooHistFunc("template_P3", "template_P3", *x, *dh_P3);
    auto template_P9 = new RooHistFunc("template_P9", "template_P9", *x, *dh_P9);
    //auto template_P5 = new RooHistFunc("template_P5", "template_P5", *x, *dh_P5);
    //auto template_P7 = new RooHistFunc("template_P7", "template_P7", *x, *dh_P7);

    template_list = new RooArgList();
    template_list->add(*template_P2);
    template_list->add(*template_P3);
    //template_list->add(*template_P5);
    //template_list->add(*template_P7);
    template_list->add(*template_P9);

    A2 = new RooRealVar("A2", "A_2", 0.01, -10.0, 10.0);
    A3 = new RooRealVar("A3", "A_3", 0.01, -10.0, 10.0);
    //A5 = new RooRealVar("A5", "A_5", 0.01, -100000.0, 100000.0);
    //A7 = new RooRealVar("A7", "A_7", 0.01, -100000.0, 100000.0);
    //A9 = new RooRealVar("Norm", "A_9", 0.01, 0.0, 100000.0);
    A9 = new RooFormulaVar("Norm", "1.0 -@0 -@1", RooArgList(*A2, *A3));

    coeff_list = new RooArgList();
    coeff_list->add(*A2);
    coeff_list->add(*A3);
    //coeff_list->add(*A5);
    //coeff_list->add(*A7);
    coeff_list->add(*A9);

    mParList.push_back("A2");
    mParList.push_back("A3");
    //mParList.push_back("A5");
    //mParList.push_back("A7");

    mCompList.push_back("template_P2");
    mCompList.push_back("template_P3");
    //mCompList.push_back("template_P5");
    //mCompList.push_back("template_P7");
    mCompList.push_back("template_P9");


    model = new RooRealSumPdf("template_phi_pdf", "template_phi_pdf", *template_list, *coeff_list);

}

/*std::vector<std::string> Hist_Phi::getParList()
{
    return mParList;
}*/

// helper functions
std::vector<float> calculateRealValue(float val, float err, float Norm_val, float Norm_err)
{
    float realvar = val/Norm_val;
    float realerr = pow(err/val, 2) + pow(Norm_err/Norm_val, 2);
    realerr = std::sqrt(realerr)*fabs(realvar);
    return std::vector<float>{realvar, realerr};
}