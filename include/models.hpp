#ifndef _MODELS_HPP_
#define _MODELS_HPP_

#include "RooArgList.h"
#include "RooFitResult.h"
#include "RooRealSumPdf.h"
#include "RooRealVar.h"
#include "TF1.h"
#include "TH1.h"
#include "TString.h"
#include "TH1F.h"
#include "RooRealSumFunc.h"
#include "RooFormulaVar.h"
#include <map>
#include <sstream>
#include <string>
#include <vector>


// TODO: Change to factory mode


class Model_Base
{
public:
    Model_Base()=default;
    Model_Base(Model_Base &&) = default;
    Model_Base(const Model_Base &) = default;
    Model_Base &operator=(Model_Base &&) = default;
    Model_Base &operator=(const Model_Base &) = default;
    ~Model_Base()=default;

    // RooRealSumFunc* GetModel();
    RooRealSumPdf* GetModel();
    RooRealVar* GetVar();
    // RooFitResult* GetResultPrt();
    // virtual int getPar(std::map<std::string, float> &mPar, std::map<std::string, float> &mErr);
    std::vector<std::string> getParList();
    std::vector<std::string> getCompList();

protected:
    RooRealVar* x;
    RooRealSumPdf* model;
    //RooRealSumFunc* model;
    // RooFitResult* mRes;
    std::vector<std::string> mParList;
    std::vector<std::string> mCompList;
};

class Func_Ctheta : public Model_Base
{
public:
    Func_Ctheta();
    Func_Ctheta(Func_Ctheta &&) = default;
    Func_Ctheta(const Func_Ctheta &) = default;
    Func_Ctheta &operator=(Func_Ctheta &&) = default;
    Func_Ctheta &operator=(const Func_Ctheta &) = default;
    ~Func_Ctheta() {};

    // int getPar(std::map<std::string, float> &mPar, std::map<std::string, float> &mErr);
    //std::vector<std::string> getParList();
    //std::vector<std::string> getCompList();

private:
    TF1 *f_P0;
    TF1 *f_P4;
    TF1 *f_P8;

    RooRealVar *A0;
    RooRealVar *A4;
    RooFormulaVar *A8;
    //RooRealVar *A8;

    RooArgList *func_list;
    RooArgList *coeff_list;

    //std::vector<std::string> mParList;
};

class Func_Phi : public Model_Base
{
public:
    Func_Phi();
    Func_Phi(Func_Phi &&) = default;
    Func_Phi(const Func_Phi &) = default;
    Func_Phi &operator=(Func_Phi &&) = default;
    Func_Phi &operator=(const Func_Phi &) = default;
    ~Func_Phi() {};

    // int getPar(std::map<std::string, float> &mPar, std::map<std::string, float> &mErr);
    //std::vector<std::string> getParList();

private:
    TF1 *f_P2;
    TF1 *f_P3;
    TF1 *f_P9;

    RooRealVar *A2;
    RooRealVar *A3;
    //RooRealVar *A9;
    RooFormulaVar *A9;

    RooArgList *func_list;
    RooArgList *coeff_list;

    //std::vector<std::string> mParList;
};

class Hist_Ctheta : public Model_Base
{
public:
    Hist_Ctheta() = default;
    Hist_Ctheta(std::string template_path, std::string template_name);
    Hist_Ctheta(Hist_Ctheta &&) = default;
    Hist_Ctheta(const Hist_Ctheta &) = default;
    Hist_Ctheta &operator=(Hist_Ctheta &&) = default;
    Hist_Ctheta &operator=(const Hist_Ctheta &) = default;
    ~Hist_Ctheta() {};

    // int getPar(std::map<std::string, float> &mPar, std::map<std::string, float> &mErr);
    //std::vector<std::string> getParList();

private:
    TH1D *hist_P0;
    TH1D *hist_P4;
    TH1D *hist_P8;

    RooRealVar *A0;
    RooRealVar *A4;
    //RooRealVar *A8;
    RooFormulaVar *A8;

    RooArgList *template_list;
    RooArgList *coeff_list;

    //std::vector<std::string> mParList;
};

class Hist_Phi : public Model_Base
{
public:
    Hist_Phi() = default;
    Hist_Phi(std::string template_path, std::string template_name);
    Hist_Phi(Hist_Phi &&) = default;
    Hist_Phi(const Hist_Phi &) = default;
    Hist_Phi &operator=(Hist_Phi &&) = default;
    Hist_Phi &operator=(const Hist_Phi &) = default;
    ~Hist_Phi() {};

    // int getPar(std::map<std::string, float> &mPar, std::map<std::string, float> &mErr);
    //std::vector<std::string> getParList();

private:
    TH1D *hist_P2;
    TH1D *hist_P3;
    TH1D *hist_P9;
    //TH1D *hist_P5;
    //TH1D *hist_P7;

    RooRealVar *A2;
    RooRealVar *A3;
    //RooRealVar *A5;
    //RooRealVar *A7;
    //RooRealVar *A9;
    RooFormulaVar *A9;

    RooArgList *template_list;
    RooArgList *coeff_list;

    //std::vector<std::string> mParList;
};

// RooRealSumPdf *Func_ctheta(RooRealVar *ctheta);
// RooRealSumPdf *Func_phi(RooRealVar *phi);

// just for test purpose
TString Welcome();

// returns [0] value and [1] error
std::vector<float> calculateRealValue(float val, float err, float Norm_val, float Norm_err);

#endif // !1