#ifndef _APP_HPP_
#define _APP_HPP_

#include "GuiTypes.h"
#include "RooFitResult.h"
#include "models.hpp"
#include "TFile.h"
#include "TH1F.h"
#include <map>
#include <string>
#include <vector>

enum Model_Type
{
    FUNCTION_COS_THETA,
    FUNCTION_PHI,
    HIST_TEMPLATE_COS_THETA,
    HIST_TEMPLATE_PHI
};

struct FitConfig
{
    Model_Type type;
    std::map<std::string, std::string> hist_list;
    std::string template_file;
    std::map<std::string, std::string> template_list;
};


class Application
{
public:
    Application() = default;
    Application(TFile* infile);
    Application(Application &&) = default;
    Application(const Application &) = default;
    Application &operator=(Application &&) = default;
    Application &operator=(const Application &) = default;
    ~Application();

    // int init(TFile* rfile, const char* model_name);
    int prepareModel(std::string m_name,std::string model_name);
    int prepareModel(std::string m_name, FitConfig* config);
    int runFit(const char* hist_name);
    int getResult(TFile* outfile);
    // int finalizeResult(TFile* outfile);
    // void reset();
private:
    TFile* rfile;
    std::map<std::string, Model_Base*> vec_model;
    std::map<std::string, RooFitResult*> vec_result;
    std::vector<std::string> vec_name;
    std::string current_name;
};

std::map<std::string, FitConfig*> getConfigList(std::string fpath="");

#endif // !1