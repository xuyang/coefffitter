#include "TFile.h"
#include "models.hpp"
#include <iostream>
#include <map>
#include <ostream>
#include <string>
#include "app.hpp"



// TODO: maybe a config parser if needed
/*std::map<std::string, std::string> ctheta_hist = {
    {"ctheta_bin1", "h_ctheta_bin1"},
    {"ctheta_bin2", "h_ctheta_bin2"},
    {"ctheta_bin3", "h_ctheta_bin3"},
    {"ctheta_bin4", "h_ctheta_bin4"},
    {"ctheta_bin5", "h_ctheta_bin5"}
};

std::map<std::string, std::string> phi_hist = {
    {"phi_bin1", "h_phi_bin1"},
    {"phi_bin2", "h_phi_bin2"},
    {"phi_bin3", "h_phi_bin3"},
    {"phi_bin4", "h_phi_bin4"},
    {"phi_bin5", "h_phi_bin5"}
};*/

std::map<std::string, std::string> ctheta_hist = {
    {"ctheta_bin1", "h_ctheta_down_bin1"},
    {"ctheta_bin2", "h_ctheta_down_bin2"},
    {"ctheta_bin3", "h_ctheta_down_bin3"},
    {"ctheta_bin4", "h_ctheta_down_bin4"},
    {"ctheta_bin5", "h_ctheta_down_bin5"}
};

std::map<std::string, std::string> phi_hist = {
    {"phi_bin1", "h_phi_down_bin1"},
    {"phi_bin2", "h_phi_down_bin2"},
    {"phi_bin3", "h_phi_down_bin3"},
    {"phi_bin4", "h_phi_down_bin4"},
    {"phi_bin5", "h_phi_down_bin5"}
};

/*std::map<std::string, std::string> ctheta_hist = {
    {"ctheta_bin1", "h_ctheta_up_bin1"},
    {"ctheta_bin2", "h_ctheta_up_bin2"},
    {"ctheta_bin3", "h_ctheta_up_bin3"},
    {"ctheta_bin4", "h_ctheta_up_bin4"},
    {"ctheta_bin5", "h_ctheta_up_bin5"}
};

std::map<std::string, std::string> phi_hist = {
    {"phi_bin1", "h_phi_up_bin1"},
    {"phi_bin2", "h_phi_up_bin2"},
    {"phi_bin3", "h_phi_up_bin3"},
    {"phi_bin4", "h_phi_up_bin4"},
    {"phi_bin5", "h_phi_up_bin5"}
};*/



int main(int argc, char** argv)
{
    TString s = Welcome();
    std::cout << s.Data() << std::endl;

    // const char* hist_name = "h_ctheta_bin5";
    // const char* model_name = "Func_Ctheta";
    //const char* fpath = "/home/shana/workarea/ZGammaJet/fitStudies_1013/mchists_1D_8bins.root";
    //const char* fpath = "/home/shana/workarea/ZGammaJet/fitStudies_1112/unfolded_fullspace.root";
    //const char* fpath = "/home/shana/workarea/ZGammaJet/fitStudies_1115/test.root";
    //const char* fpath = "/home/shana/workarea/data/ZGammaJet/unfolded_1202/unfolded.root";

    /*if (argc == 1)
    {
        std::cout << "you must choose a job type, see list below" << std::endl;
        std::cout << " 0. function cos theta" << std::endl;
        std::cout << " 1. function phi" << std::endl;
        std::cout << " 2. hist template cos theta" << std::endl;
        std::cout << " 3. hist template phi" << std::endl;

        return -1;
    }*/

    if (argc < 4)
    {
        std::cout << "You must give a config file and a job type as well as an input file" << std::endl;
        std::cout << "for example: [exec] config.txt Func_Ctheta input.root" << std::endl;
        std::cout << "second arg must be one of the keys specified in the config" << std::endl;
        
        return -1;
    }
    auto config_list = getConfigList(argv[1]);
    /*char arg = *(argv[1]);
    Model_Type type;
    type = Model_Type(int(arg)-48);*/
    for (auto a : config_list)
    {
        std::cout<<a.first<<std::endl;
    }
    auto config = config_list.at(argv[2]);
    std::cout<<"1"<<std::endl;


    auto fpath = argv[3];
    auto rfile = new TFile(fpath, "READONLY");
    auto app = new Application(rfile);

    std::string model_name;
    auto type = config->type;
    auto hist_list = config->hist_list;

    /*switch (type) {
    case FUNCTION_COS_THETA:
        model_name = "Func_Ctheta";
        break;
    case FUNCTION_PHI:
        model_name = "Func_Phi";
        break;
    case HIST_TEMPLATE_COS_THETA:
        model_name = "Hist_Ctheta";
        break;
    case HIST_TEMPLATE_PHI:
        model_name = "Hist_Phi";
        break;
    default:
        std::cout << "given pattern is not found" << std::endl;
        return -1;
    }*/

    TFile *outfile = new TFile("Ai_output.root", "UPDATE");

    for (auto job : hist_list)
    {
        std::string hist_name = job.second;
        std::string job_name = job.first;
        /*if (model_name == "Func_Ctheta" || model_name == "Func_Phi")
        {
            app->prepareModel(job_name, model_name);
        }
        else
        {
            app->prepareModel(job_name, config);
        }*/
        app->prepareModel(job_name, config);
        app->runFit(hist_name.c_str());
    }
    /*if (type == FUNCTION_COS_THETA || type == HIST_TEMPLATE_COS_THETA)
    {
        for (auto job : ctheta_hist)
        {
            std::string hist_name = job.second;
            std::string job_name = job.first;
            app->prepareModel(job_name, model_name);
            app->runFit(hist_name.c_str());
        }
    }*/
    /*else
    {
        for (auto job : phi_hist)
        {
            std::string hist_name = job.second;
            std::string job_name = job.first;
            app->prepareModel(job_name, model_name);
            app->runFit(hist_name.c_str());
        }
    }*/
    app->getResult(outfile);
    outfile->Close();
    rfile->Close();

 /*   if (app->init(rfile, hist_name, model_name) == 1)
    {
        if (app->prepareModel() == 1)
        {
            app->runFit();
        }
        else
        {
            return -1;
        }
    }
    else
    {
        return -1;
    } */

    return 0;
}
